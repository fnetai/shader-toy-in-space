
import ShaderToy from '../src';

export default ({ container }) => {
    return (args) => ShaderToy({ container, stats: false, mouse: true, ...args, })
};