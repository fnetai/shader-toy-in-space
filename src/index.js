import ShaderToy from "@fnet/shader-toy";

// https://www.shadertoy.com/view/sldGDf
// https://cineshader.com/view/sldGDf
import fshader from "./fshader.glsl";

export default async (args) => {
    await ShaderToy({ fshader, canvas: { backgroundColor: "#000" }, stats: true, ...args })
}